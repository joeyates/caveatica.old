use Mix.Config

config :nerves_firmware_ssh,
  authorized_keys: [
    File.read!(Path.join(System.user_home!, ".ssh/id_rsa.pub"))
  ]

config :logger, backends: [RingLogger]

config :shoehorn,
  init: [:nerves_runtime, :nerves_init_gadget],
  app: Mix.Project.config()[:app]

environment_config = "#{Mix.env}.exs"
if File.exists?(Path.join("config", environment_config)) do
  import_config environment_config
end

environment_secrets = "#{Mix.env}.secrets.exs"
if File.exists?(Path.join("config", environment_secrets)) do
  import_config environment_secrets
end
