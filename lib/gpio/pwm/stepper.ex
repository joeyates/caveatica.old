defmodule Gpio.Pwm.Stepper do
  use GenServer
  require Logger

  alias ElixirALE.GPIO

  # state:
  #   * pins - the GPIO ids of the pins,
  #   * steps - an array indicating, for each phase, which pins to set high,
  #   * phase - the current index for steps,
  #   * direction - the direction of rotation: `[:none|:forwards|:backwards]`,
  #   * period - interval between pulses (in ms),
  #   * links - pids of the individual GPIO pins.

  # actions:
  #   * state: get state
  #   * forwards: run forwards
  #   * backwards: run backwards
  #   * stop: stop (if running)

  def start_link(%{pins: pins} = args) do
    # TODO: check steps is a list of lists, where each sublist
    # has the same number of items as the pins list.
    links = Enum.map(
      pins,
      fn (num) ->
        {:ok, pid} = GPIO.start_link(num, :output)
        pid
      end
    )
    state = Map.merge(
      %{direction: :none, period: nil, phase: nil, links: links}, args
    )
    GenServer.start_link(__MODULE__, state, name: :gpio_pwm_stepper)
  end

  def state, do: GenServer.call(:gpio_pwm_stepper, {:state})

  def start(:forwards, period) do
    # TODO: stop first
    GenServer.cast(:gpio_pwm_stepper, {:forwards, period})
    GenServer.cast(:gpio_pwm_stepper, {:step})
  end
  def start(:backwards, period) do
    # TODO: stop first
    GenServer.cast(:gpio_pwm_stepper, {:backwards, period})
    GenServer.cast(:gpio_pwm_stepper, {:step})
  end

  def stop, do: GenServer.cast(:gpio_pwm_stepper, {:stop})

  def step, do: GenServer.cast(:gpio_pwm_stepper, {:step})

  @impl GenServer
  def init(args) do
    {:ok, args}
  end

  @impl GenServer
  def handle_call({:state}, _from, state) do
    {:reply, state, state}
  end

  @impl GenServer
  def handle_cast({:forwards, period}, state) do
    new_state = Map.merge(
      state,
      %{direction: :forwards, period: period, phase: 0}
    )
    {:noreply, new_state}
  end
  def handle_cast({:backwards, period}, state) do
    new_state = Map.merge(
      state,
      %{direction: :backwards, period: period, phase: 0}
    )
    {:noreply, new_state}
  end
  def handle_cast({:stop}, %{links: links} = state) do
    step = [0, 0, 0, 0]
    set_pins(step, links)
    {:noreply, Map.merge(state, %{direction: :none, period: nil, phase: nil})}
  end
  def handle_cast({:step}, %{direction: :none} = state) do
    {:noreply, state}
  end
  def handle_cast({:step}, state) do
    %{steps: steps, links: links, phase: phase, direction: direction, period: period} = state
    # Prepare for next tick as soon as possible
    Process.send_after(self(), {:next}, period)
    step_count = length(steps)
    Logger.info "step, phase: #{phase}, direction: #{direction}"
    step = Enum.at(steps, phase)
    set_pins(step, links)
    next_phase = phase_after(phase, step_count, direction)
    {:noreply, put_in(state, [:phase], next_phase)}
  end

  @impl GenServer
  def handle_info({:next}, state) do
    step()
    {:noreply, state}
  end

  defp set_pins(step, links) do
    step
    |> Enum.zip(links)
    |> Enum.each(
      fn {value, pid} ->
        GPIO.write(pid, value)
      end
    )
  end

  defp phase_after(phase, step_count, :forwards) do
    if phase + 1 >= step_count do
      0
    else
      phase + 1
    end
  end
  defp phase_after(0, step_count, :backwards), do: step_count - 1
  defp phase_after(phase, _step_count, :backwards), do: phase - 1
end
